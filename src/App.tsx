import React from 'react';
import Header from "./components/Header";
import './assets/fonts/Ubuntu.scss'
import About from "./components/About";
import Timeline from "./components/Timeline";
import Skills from "./components/Skills";
import Portfolio from "./components/Portfolio";
import Interest from "./components/Interest";
import Footer from "./components/Footer";

const App = () => {
    return (
        <>
            <Header/>
            <About/>
            <Timeline/>
            <Skills/>
            <Portfolio/>
            <Interest/>
            <Footer/>
        </>
    );
}

export default App;
