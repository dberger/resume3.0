import React, {FunctionComponent} from 'react';
import "../assets/scss/Timeline.scss"

const Timeline: FunctionComponent = () => {
    return (
        <section className={'timeline'} id={'timeline'}>
            <div id="timeline-content">
                <h1>Expériences</h1>


                <ul className="timeline">
                    <li className="event" data-date="sept 2019 - Aujourd'hui">
                        <img className={'event__logo'} alt={'SFI'}
                             src="./img/jobs/sfi.png"/>
                        <div>
                            <p className={'event__company'}>SFI Multimédia</p>
                            <p className={'event__job'}>Développeur junior</p>
                            <p className={'event__info'}>Développement d'un CRM avec une architecture en micro-services
                                (API REST) en
                                <span className={'event__techno'}> PHP 7.2</span> / <span className={'event__techno'}>Symfony 4 (ApiPlatform)</span> / <span
                                    className={'event__techno'}>React.js</span></p>
                        </div>
                    </li>
                    <li className="event" data-date="sept 2018 - sept 2019">
                        <div className={'multiple'}>
                            <div className={'event__secondary'}>
                                <img className={'event__logo'} alt={'SFI'}
                                     src="./img/jobs/sfi.png"/>
                                <div>
                                    <p className={'event__company'}>SFI Multimédia</p>
                                    <p className={'event__job'}>Apprenti développeur</p>
                                    <p className={'event__info'}>Contrat en alternance, développement de plusieurs
                                        back-offices en
                                        <span className={'event__techno'}> PHP 5 et 7</span>, et
                                        <span className={'event__techno'}> Symfony 2 et 3</span></p>
                                </div>
                            </div>
                            <div className={'event__secondary'}>
                                <img className={'event__logo'} alt={'UCA'}
                                     src="./img/jobs/uca.png"/>
                                <div>
                                    <p className={'event__company'}>Université Clermont - Auvergne</p>
                                    <p className={'event__job'}>Licence Professionnelle Développement d’applications
                                        intranet/internet. </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li className="event" data-date="avril 2018 - juin 2018">
                        <img className={'event__logo'} alt={'SFI'}
                             src="./img/jobs/sfi.png"/>
                        <div>
                            <p className={'event__company'}>SFI Multimédia</p>
                            <p className={'event__job'}>Stagiaire</p>
                            <p className={'event__info'}>Stagiaire, développement de plusieurs back-offices en
                                <span className={'event__techno'}> PHP 5 et 7</span>, et
                                <span className={'event__techno'}> Symfony 2 et 3</span></p>
                        </div>
                    </li>
                    <li className="event" data-date="2016 - 2018">
                        <img className={'event__logo'} alt={'UCA'}
                             src="./img/jobs/uca.png"/>
                        <div>
                            <p className={'event__company'}>Université Clermont - Auvergne</p>
                            <p className={'event__job'}>DUT Informatique</p>
                        </div>
                    </li>
                    <li className="event" data-date="2016">
                        <img className={'event__logo'} alt={'St-Aubrin'}
                             src="./img/jobs/aubrin.jpg"/>
                        <div>
                            <p className={'event__company'}>Lycée Saint-Aubrin, Montbrison</p>
                            <p className={'event__job'}>BAC Série Scientifique SVT spécialité Mathématiques</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    );
};

export default Timeline;
