import React, {FunctionComponent} from 'react';
import '../assets/scss/Footer.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowUp} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-scroll";
import {faGitlab} from "@fortawesome/free-brands-svg-icons";

const Footer: FunctionComponent = () => {
    return (
        <footer>
            <ul className="copyright">
                <li>&copy; Copyright 2020 Dimitri BERGER.</li>
                <li>This resume is a rewritten version of <a target="_blank" rel="noreferrer" title="react-nice-resume"
                                                             href="https://github.com/nordicgiant2/react-nice-resume">react-nice-resume </a>
                    using TypeScript, React and SASS available on my <a target="_blank" rel="noreferrer"
                                                                        href={"https://gitlab.com/dberger/resume3.0"}>
                        <FontAwesomeIcon icon={faGitlab}/> GitLab
                    </a>
                </li>
            </ul>

            <div id="go-top">
                <Link
                    activeClass="current"
                    to="home"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >
                    <FontAwesomeIcon icon={faArrowUp}/>
                </Link>
            </div>
        </footer>
    );
};

export default Footer;
