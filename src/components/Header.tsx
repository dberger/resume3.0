import ParticlesBg from 'particles-bg';
import React, {FunctionComponent} from 'react';
import "../assets/scss/Header.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faAngleDown, faEnvelope} from '@fortawesome/free-solid-svg-icons'
import {faGitlab, faGithub, faLinkedin} from '@fortawesome/free-brands-svg-icons'
import {Link} from "react-scroll";

const Header: FunctionComponent = () => {
    return (
        <header id="home">
            <ParticlesBg type="circle" bg={true}/>
            <nav id="nav-wrap">
                <Link
                    activeClass="current"
                    to="home"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >Accueil</Link>
                <Link
                    activeClass="current"
                    to="about"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >A propos</Link>
                <Link
                    activeClass="current"
                    to="timeline"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >Expériences</Link>
                <Link
                    activeClass="current"
                    to="skills"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >Compétences</Link>
                <Link
                    activeClass="current"
                    to="portfolio"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >Portfolio</Link>
                <Link
                    activeClass="current"
                    to="interest"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                >Intérêts</Link>
            </nav>

            <div className="row banner">

                <div className="banner-text">
                    <h1 className="responsive-headline">Dimitri <span className={'lastname'}>BERGER</span></h1>
                    <h3>Développeur Junior à <strong>SFI
                        MULTIMEDIA</strong> sur <strong>Saint-Etienne</strong> - <FontAwesomeIcon icon={faEnvelope}/> <a
                        href={'mailto:dimitri@dimitri-berger.fr'}>dimitri@dimitri-berger.fr</a></h3>
                    <hr/>
                    <div className="social-icons">
                        <a target="_blank" rel="noopener noreferrer"
                           href="https://www.linkedin.com/in/dimitri-berger-426b7a13a/"><FontAwesomeIcon
                            icon={faLinkedin}/></a>
                        <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/dberger"><FontAwesomeIcon
                            icon={faGitlab}/></a>
                        <a target="_blank" rel="noopener noreferrer" href="https://github.com/ylony"><FontAwesomeIcon
                            icon={faGithub}/></a>
                    </div>
                </div>
            </div>

            <p className="scrolldown">
                <Link
                    activeClass="current"
                    to="about"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                ><FontAwesomeIcon icon={faAngleDown}/></Link>
            </p>

        </header>
    );
};

export default Header;
