import React, {FunctionComponent, useEffect, useState} from "react";

interface SkillBarsProps {
    hue: string,
    saturation: string,
    skills: Skill[],
}

export interface Skill {
    type: string,
    level: number
}

type Props = SkillBarsProps;

const SkillBars: FunctionComponent<Props> = (props) => {
    const [collapsed, setCollapsed] = useState(true);
    const {hue, saturation, skills} = props;

    useEffect(() => {
        setTimeout(() => {
            setCollapsed(false)
        }, 1000);
    }, []);

    return (
        <>
            <div id="app" className={`container ${collapsed ? 'collapsed' : ''}`}>
                <ul className="skills">
                    {skills.map((skill, index) =>
                        <li
                            key={skill.type}
                            style={{
                                width: `${skill.level}%`,
                                backgroundColor: `hsl(${hue}, ${saturation}%, ${100 / (index + 3.5)}%)`
                            }}
                        >
                            <p>{skill.type}<span>{skill.level}</span></p>
                        </li>
                    )}
                </ul>
            </div>
        </>
    )
}

export default SkillBars
