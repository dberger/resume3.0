import React, {FunctionComponent} from 'react';
import '../assets/scss/Interest.scss'

const Interest: FunctionComponent = () => {
    return (
        <>
            <hr/>
            <section className="interest-section p-3 p-lg-5 d-flex align-items-center" id="interest">
                <div className="w-100">
                    <h1 className="mb-5">
                        Intérêts
                    </h1>
                    <div className="interest-container">
                        <div className="disintegration-target">
                            <img className="interest-img" src="./img/interests/f1.jpg" alt={'F1'}/>
                            <p className="interest-title">
                                Formule 1
                            </p>
                        </div>
                        <div className="disintegration-target">
                            <img className="interest-img" src="./img/interests/marvel.jpg" alt={'Marvel'}/>
                            <p className="interest-title">
                                Univers Marvel
                            </p>
                        </div>
                        <div className="disintegration-target">
                            <img className="interest-img" src="./img/interests/travel.jpg" alt={'Voyage'}/>
                            <p className="interest-title">
                                Voyager
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <hr className="m-0"/>
        </>
    );
};

export default Interest;
