import React, {FunctionComponent} from 'react';
import "../assets/scss/Skills.scss"
import SkillBars, {Skill} from "./SkillBars";

const skillsData: Skill[] = [
    {
        type: 'PHP 7.4 / Symfony 2-3-4 + ApiPlatform',
        level: 90,
    },
    {
        type: 'JavaScript (vanilla, jQuery), TypeScript, React.js, React Ionic (Mobile), React Native (Mobile)',
        level: 85,
    },
    {
        type: 'MySQL / Oracle',
        level: 75,
    },
    {
        type: 'Docker, VirtualBox, Vagrant',
        level: 75,
    },
    {
        type: 'ElasticSearch / MongoDB / NoSql',
        level: 70,
    },
    {
        type: 'CI/CD Bitbucket/Gitlab',
        level: 70,
    },
    {
        type: 'C# / .NET + XAML / WPF',
        level: 70,
    },
    {
        type: 'Java / Android',
        level: 65,
    },
    {
        type: 'HTML / CSS / SASS / LESS',
        level: 65,
    },
    {
        type: 'Kubernetes',
        level: 60,
    },
    {
        type: 'Ruby',
        level: 60,
    },
    {
        type: 'C',
        level: 60,
    },
    {
        type: 'Photoshop',
        level: 50,
    },
    {
        type: 'Anglais - TOEIC 925 (2018)',
        level: 90,
    },
    {
        type: 'Français',
        level: 100,
    },
    {
        type: 'Permis B',
        level: 100,
    }
]

const Skills: FunctionComponent = () => {
    return (
        <section className={'skills-container'} id={'skills'}>
            <hr/>
            <h1>Compétences</h1>
            <SkillBars hue="370" saturation="50" skills={skillsData}/>
        </section>
    );
};

export default Skills;
