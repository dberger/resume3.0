import React, {FunctionComponent} from 'react';
import "../assets/scss/About.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faDownload} from '@fortawesome/free-solid-svg-icons'
import portrait from '../assets/img/profile.jpg'

const About: FunctionComponent = () => {
    return (
        <section id="about">
            <div className="row-container">
                <div className="profile-container">
                    <img className="profile-pic" src={portrait} alt="Portrait"/>
                </div>
                <div className="about-container">
                    <h2>A propos</h2>

                    <p>
                        Je suis actuellement développeur dans l'entreprise SFI Multimédia à Saint-Etienne, je travaille
                        sur un outil intégrant une architecture de type micro-services en PHP/Symfony 4 et un front en
                        React. Je travaille également sur des plateformes de gestion en Symfony 2 et 3
                    </p>
                    <div className="contact-container">
                        <div className="columns contact-details">
                            <h2>Coordonnées</h2>
                            <div className="address">
                                <p>Dimitri BERGER</p>
                                <p>20 rue des gourlettes</p>
                                <p>Clermont-Ferrand, 63000</p>
                                <p><a href={'mailto:dimitri@dimitri-berger.fr'}>dimitri@dimitri-berger.fr</a></p>
                                <p>06.52.36.42.45</p>
                            </div>
                        </div>
                        <div className="columns download">
                            <p>
                                <a target='_blank' href='./uploads/cv_2.2.pdf' className="button"><FontAwesomeIcon
                                    icon={faDownload}/>Télécharger le CV</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    );
};

export default About;
